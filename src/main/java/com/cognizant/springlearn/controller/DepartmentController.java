package com.cognizant.springlearn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.model.Department;
import com.cognizant.springlearn.model.Employee;
import com.cognizant.springlearn.service.DepartmentService;
import com.cognizant.springlearn.service.EmployeeService;

@RestController
public class DepartmentController {

	@Autowired
	private DepartmentService deptService;

	@GetMapping("/departments")
	public List<Department> getAllEmployees() {
		return deptService.getAllDepartments();
	}
}
