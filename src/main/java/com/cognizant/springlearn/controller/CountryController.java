package com.cognizant.springlearn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cognizant.springlearn.exception.CountryNotFoundException;
import com.cognizant.springlearn.model.Country;
import com.cognizant.springlearn.service.CountryService;

@RestController
@RequestMapping("/countries")
public class CountryController {
	ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
	private static final Logger LOGGER = LoggerFactory.getLogger(CountryController.class);
	@Autowired
	private CountryService service;

	public CountryController() {
		LOGGER.info("Inside CountryController Constructor");
	}

	@RequestMapping("/country")
	public Country getCountryIndia() {

		Country country = context.getBean("country", Country.class);
		return country;

	}

	@SuppressWarnings("unchecked")
	@GetMapping()
	public List<Country> getAllCountries() {
		List<Country> countries = (List<Country>) context.getBean("countryList");
		return countries;
	}

	@GetMapping("/{code}")
	public Country getCountry(@PathVariable String code) throws CountryNotFoundException {

		Country country = service.getCountry(code);

		return country;

	}

	@PostMapping("/coma")
	public Country addCountry(@RequestBody @Valid Country country) {
		LOGGER.info("Start- In addCountry()");
		/*
		 * // Create validator factory ValidatorFactory factory =
		 * Validation.buildDefaultValidatorFactory(); Validator validator =
		 * factory.getValidator();
		 * 
		 * // Validation is done against the annotations defined in country bean
		 * Set<ConstraintViolation<Country>> violations = validator.validate(country);
		 * List<String> errors = new ArrayList<String>();
		 * 
		 * // Accumulate all errors in an ArrayList of type String for
		 * (ConstraintViolation<Country> violation : violations) {
		 * errors.add(violation.getMessage()); }
		 * 
		 * // Throw exception so that the user of this web service receives appropriate
		 * // error message if (violations.size() > 0) { throw new
		 * ResponseStatusException(HttpStatus.BAD_REQUEST, errors.toString());
		 * 
		 * }
		 */
		LOGGER.debug("Country={}", country);

		return country;
	}

}
