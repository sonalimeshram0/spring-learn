package com.cognizant.springlearn.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Employee;
import com.cognizant.springlearn.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService empService;

	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return empService.getAllEmployees();
	}
	
	@PutMapping("/employees/put")
	public String updateEmployee(@RequestBody @Valid Employee employee) throws EmployeeNotFoundException {
		empService.updateEmployee(employee);
		return "Employee updated";
	}
}
