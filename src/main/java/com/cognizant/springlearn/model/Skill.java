package com.cognizant.springlearn.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Skill {

	@NotNull
	@Pattern(regexp = "[0-9]+")
	private int id;
	@NotNull
	@NotBlank()
	@Min(value = 1)
	@Max(value = 30)
	private String name;

	public Skill() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Skill(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Skill [id=%s, name=%s]", id, name);
	}

}
