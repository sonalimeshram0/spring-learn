package com.cognizant.springlearn.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognizant.springlearn.SpringLearnApplication;

public class Country {
	private static final Logger LOGGER=LoggerFactory.getLogger(SpringLearnApplication.class);
	
	@NotNull
	@Size(min = 2,max=2,message ="Country code should be 2 characters")
	private String code;
	private String name;
	
	public Country(){
		LOGGER.debug("Inside Country Constructor");
	}

	public String getCode() {
		LOGGER.info("returning code...");
		return code;
	}

	public void setCode(String code) {
		LOGGER.info("setting code to: "+code);
		this.code = code;
	}

	public String getName() {
		LOGGER.info("returning name...");
		return name;
	}

	public void setName(String name) {
		LOGGER.info("setting name to: "+name);
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Country [code=%s, name=%s]", code, name);
	}
	
	
	
}
