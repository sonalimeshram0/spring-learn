package com.cognizant.springlearn.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Employee {

	@NotNull
	@Pattern(regexp = "[0-9]+")
	private int id;
	@NotNull
	@NotBlank()
	@Min(value = 1)
	@Max(value = 30)
	private String name;
	@NotNull
	@Min(value = 0,message = "salary should not be less than 0")
	private double salary;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy")
	private Date dob;
	@NotNull
	private boolean permanent;
	private Department department;
	private List<Skill> skills;
	public Employee(int id, String name, double salary, Date dob, boolean permanent) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.dob = dob;
		this.permanent = permanent;
	}
	public Employee() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public boolean isPermanent() {
		return permanent;
	}
	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public List<Skill> getSkills() {
		return skills;
	}
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	@Override
	public String toString() {
		return String.format("Employee [id=%s, name=%s, salary=%s, dob=%s, permanent=%s, department=%s, skills=%s]", id,
				name, salary, dob, permanent, department, skills);
	}
	
	
	
	
	
	
}
