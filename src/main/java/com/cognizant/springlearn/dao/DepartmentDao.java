package com.cognizant.springlearn.dao;

import java.util.ArrayList;
import java.util.List;

import com.cognizant.springlearn.model.Department;

public class DepartmentDao {

	private static List<Department> departmentList=new ArrayList<>();
	
	public DepartmentDao() {}

	public DepartmentDao(List<Department> departmentList) {
		super();
		DepartmentDao.departmentList = departmentList;
		
		//System.out.println(" departmentDao constuctor: "+DepartmentDao.departmentList);
	}

	public static List<Department> getDepartmentList() {
		return departmentList;
	}

	public static void setDepartmentList(List<Department> departmentList) {
		DepartmentDao.departmentList = departmentList;
	}
	
	
}
