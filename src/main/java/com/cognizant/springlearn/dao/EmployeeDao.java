package com.cognizant.springlearn.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cognizant.springlearn.SpringLearnApplication;
import com.cognizant.springlearn.exception.CountryNotFoundException;
import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Employee;

@Component
public class EmployeeDao {
	private static final Logger log = LoggerFactory.getLogger(EmployeeDao.class);
	private static List<Employee> employeeList = new ArrayList<Employee>();

	public EmployeeDao() {
	}

	public EmployeeDao(List<Employee> employeeList) {
		super();
		EmployeeDao.employeeList = employeeList;

		//System.out.println(" empdao constuctor: " + EmployeeDao.employeeList);
	}

	public static List<Employee> getEmployeeList() {
		return employeeList;
	}

	public static void setEmployeeList(List<Employee> employeeList) {
		EmployeeDao.employeeList = employeeList;
	}

	public static void updateEmployee(Employee emp) throws EmployeeNotFoundException {

		Employee employee = EmployeeDao.employeeList
				.stream()
				.filter(e -> e.getId() == emp.getId())
				.findAny()
				.map(e -> e = emp)
				.orElseThrow(() -> new EmployeeNotFoundException());
		
		log.debug("employee={}",employee);
		
	}

}
