package com.cognizant.springlearn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.springlearn.dao.EmployeeDao;
import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Employee;

@Service
public class EmployeeService {



	@Transactional
	public List<Employee> getAllEmployees() {
		//ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		//EmployeeDao dao=context.getBean("employeeList",EmployeeDao.class);

		System.out.println(EmployeeDao.getEmployeeList());

		return EmployeeDao.getEmployeeList();
	}
	
	public void updateEmployee(Employee e) throws EmployeeNotFoundException  {
		EmployeeDao.updateEmployee(e);
	}
}
