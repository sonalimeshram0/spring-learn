package com.cognizant.springlearn.service;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.cognizant.springlearn.exception.CountryNotFoundException;
import com.cognizant.springlearn.model.Country;

@Service
public class CountryService {

	@SuppressWarnings("unchecked")
	public Country getCountry(String code) throws CountryNotFoundException {

		ApplicationContext ctx = new ClassPathXmlApplicationContext("country.xml");
		List<Country> countries = (List<Country>) ctx.getBean("countryList");
		Country country = countries.stream().filter(c -> c.getCode().equalsIgnoreCase(code)).findFirst()
				.orElseThrow(() -> new CountryNotFoundException());

		return country;
	}
}
