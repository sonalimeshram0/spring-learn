package com.cognizant.springlearn.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cognizant.springlearn.dao.DepartmentDao;
import com.cognizant.springlearn.model.Department;

@Service
public class DepartmentService {

	@Transactional
	public List<Department> getAllDepartments() {
		//ApplicationContext context = new ClassPathXmlApplicationContext("employee.xml");
		//EmployeeDao dao=context.getBean("employeeList",EmployeeDao.class);

		System.out.println(DepartmentDao.getDepartmentList());

		return DepartmentDao.getDepartmentList();
	}
}
