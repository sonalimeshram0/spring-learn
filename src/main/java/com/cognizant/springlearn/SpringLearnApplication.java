package com.cognizant.springlearn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cognizant.springlearn.exception.EmployeeNotFoundException;
import com.cognizant.springlearn.model.Country;
import com.cognizant.springlearn.model.Employee;
import com.cognizant.springlearn.service.DepartmentService;
import com.cognizant.springlearn.service.EmployeeService;

@SpringBootApplication
public class SpringLearnApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringLearnApplication.class);
	private static EmployeeService empservice;
	private static DepartmentService deptservice;

	public static void main(String[] args) throws EmployeeNotFoundException {
		SpringApplication.run(SpringLearnApplication.class, args);
		ApplicationContext empContext = new ClassPathXmlApplicationContext("employee.xml");
		empservice = empContext.getBean("empservice", EmployeeService.class);
		deptservice = empContext.getBean("deptservice", DepartmentService.class);
		// displayDate();
		// displayCountry();
		displayCountries();
		// displayEmployees();
		// displayDepartments();
		// updateEmployees();
	}

	public static void displayDate() {
		LOGGER.info("START");
		ApplicationContext context = new ClassPathXmlApplicationContext("date-format.xml");
		SimpleDateFormat format = context.getBean("dateFormat", SimpleDateFormat.class);
		try {
			Date date = format.parse("31/12/2018");
			LOGGER.debug("{}", date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		LOGGER.info("END");
	}

	public static void displayCountry() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		Country country = context.getBean("country", Country.class);
		Country anotherCountry = context.getBean("country", Country.class);
		LOGGER.debug("Country: {}", country);

	}

	@SuppressWarnings("unchecked")
	public static void displayCountries() {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		List<Country> list = context.getBean("countryList", ArrayList.class);

		LOGGER.debug("CountryList: {}", list);

	}

	@SuppressWarnings("unchecked")
	public static void displayEmployees() {

		LOGGER.debug("CountryList: {}", empservice.getAllEmployees());

	}

	@SuppressWarnings("unchecked")
	public static void displayDepartments() {
		

		LOGGER.debug("CountryList: {}", deptservice.getAllDepartments());

	}

	@SuppressWarnings("unchecked")
	public static void updateEmployees() throws EmployeeNotFoundException {

		empservice.updateEmployee(new Employee(2, "Mary", 5000.0, new Date(11 / 24 / 1998), true));

		LOGGER.debug("CountryList: {}", empservice.getAllEmployees());

	}

}
